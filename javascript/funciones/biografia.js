// Social media profile

// 1. USer information
const userName = 'CodingAdventure'
const fullname = 'Samir wilches'
const age =  23
const isStudent = true

// 2. Address
const address = {
    street: '123 Main Street',
    city: 'Techville',
    state: 'Codingland',
    zipCode: 54321
}

// 3. Hobbies
const hobbies = ['coding', 'reading', 'gaming']

// 4. Generating personalized bio
const personalizedBio = `Hola Soy ${fullname}.
yo tengo ${age} años.
y yo vivo ${address.city}.
yo amo ${hobbies.join(', ')}.
sigueme para mas contenido!`

// 5. print the user profile and bio
console.log(personalizedBio)