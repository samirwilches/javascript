// 1. Tipo entero y decimal
const entero = 42
const decimal =3.14
console.log(typeof entero, typeof decimal);

// 2. Notacion Cientifica
const cientifico = 5e3

// 3. Numeros Infinitos y NaN
const infinito = Infinity
const noNumero = NaN

// Operaciones Aritmeticas

// 1. Suma, Resta, Multiplicacion y Division
const Suma = 3 + 4
const Resta = 4 - 4
const Multiplicacion = 4 * 7
const Division = 16 / 2

// 2. Modulo y Exponenciacion
const Modulo = 15 % 8
const Exponenciacion = 2 ** 3

// Precision
const resultado = 0.1 + 0.2
console.log(resultado)
console.log(resultado.toFixed(1))
console.log(resultado === 0.3)

// Operaciones Avanzadas
const raizCuadrada = Math.sqrt(16)
const valorAbsoluto = Math.abs(-7)
const numeroRamdon = Math.random()
console.log(raizCuadrada)
console.log(valorAbsoluto)
console.log(numeroRamdon)

