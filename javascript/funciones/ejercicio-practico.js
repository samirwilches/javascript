// create powerfull objects
function powerfull (name, color, superpower) {
    this.name = name
    this.color = color
    this.superpower = superpower
    this.islider = false

    this.displayInfo = function () {
        console.log(`powerfull information:
        Name: ${this.name}
        color: ${this.color}
        superpower: ${this.superpower}
        ${this.islider ? 'Leader: Yes' : 'Leader: No'}
        `)
    }

    this.becomeLider = function () {
        this.islider = true
        console.log(`${this.name} has become the Lider of the league of justicy`)
    }
}

const superman = new powerfull ('Superman', 'azul', 'mucha fuerza')
const batman = new powerfull ('batman', 'negro', 'rico')
const flash = new powerfull ('flash', 'rojo', 'rapido')

superman.displayInfo()
batman.displayInfo()
flash.displayInfo()


superman.becomeLider()

superman.displayInfo()
batman.displayInfo()
flash.displayInfo()