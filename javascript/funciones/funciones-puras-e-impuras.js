// Funciones puras

// Side effects  --->   efectos secundarios

// 1. Modificar variables globales
// 2. Modificar parametros
// 3. Solicitudes Http
// 4. Imprimir mensajes en pantalla o en consola
// 5. Manipulacion del DOM 
// 6. Acceder a la hora o la fecha

function suma (a, b) {
    return a + b
}

// Funciones impuras

function suma (a, b) {
    console.log('A: ', a)
    return a + b
}

let total = 0

function sumawithsideeffect (a) {
    total += a
    return total
}

// Funcion pura

function square(x) {
    return x * x
}

function addten (y) {
    return y + 10
}

const numero = 5
const resultadofinal = addten(square(numero))
console.log(resultadofinal)