//Paso por Valor
let x = 1
let y = 'Hola'
let z = null

let a = x
let d = y
let c = z

console.log(x, y, z, a, d, c)

a = 12
d = 'platzi'
c = undefined

console.log(x, y, z, a, d, c)

//Paso por Referencia
let frutas = ['manzana']
frutas.push('pera')
console.log(frutas)

let panes = ['🥐']
let copiapanes = panes
panes.push('🥖')
console.log(panes,copiapanes)