// Null
const snoopy = null
console.log(snoopy)
console.log(typeof snoopy)

// Undefined
let name
console.log(name)

// Symbol
const uniqueId = Symbol('id')
const symbol1 = Symbol(1)
const symbol2 = Symbol(1)
console.log(symbol1 === symbol2)

const Id = Symbol('id')
let user = {}
user[Id] = '1234'
console.log(user)


// Bigint
const bigNumber = 102254854864894350000000000000n
console.log(bigNumber)

const numberOfParticles = 1000000000000000000000000n
console.log(numberOfParticles)