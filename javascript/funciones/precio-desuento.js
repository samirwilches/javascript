function calculateDiscountedPrice (price, discountPercentage) {
    const discount = (price * discountPercentage) / 100
    const pricewithdiscount = price - discount

    return pricewithdiscount
}

const price = 100
const discountPercentage = 20
const finalPrice =  calculateDiscountedPrice(price, discountPercentage)

console.log('original price: $' + price) 
console.log('discount: ' + discountPercentage + '%' )
console.log('price with discount: $' + finalPrice)