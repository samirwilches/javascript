//Creacion de strings
const primeraOpcion = 'Comillas simples'
const segundaOpcion = "Comillas dobles"
const tercerapcion = `Comillas simples`

// 1. Concatenacion: opcion +
const direccion = 'calle falsa 123'
const ciudad = 'springfield'
const direccionCompleta = 'mi direccion completa es ' + direccion + '' + ciudad
console.log(direccionCompleta)

const direccionCompletaconEspacio = 'mi direccion completa es ' + direccion + ' ' + ciudad
console.log(direccionCompletaconEspacio)


// 2. Concatenacion: template literals
const nombre = 'samir'
const juego = '⚽'
const unirnombreJuego =  `Hola soy ${nombre} y me gusta jugar ${juego}`
console.log(unirnombreJuego)

// 3. Concatenacion: join()
const primeraparte = 'Me encanta' 
const segundaparte = 'la gente que juega' 
const terceraparte = '⚽' 
const pensamiento = [primeraparte, segundaparte, terceraparte]
console.log(pensamiento.join(' 🏀 '))

// 4. concatenacion: concat()
const hobbie1 = '⚽'
const hobbie2 = '🏀'
const hobbie3 = '🥎'
const hobbies = 'Mis hobbies son: '.concat(hobbie1, ', ', hobbie2, ', ', hobbie3, '. ')
console.log(hobbies)


// Caracteres de escape
//const whatDoIDo = 'I'm software Engineer'

// 1. Escape alternativo
const escapeAlternativo= "I'm Software Engineer"

// 2. Barra invertida
const ESCAPEbarraInvertida= ' I\'m Sofware Engineer'

// 3. Template iterals
const escapeComiillaInvertida = `I\'m Sofware Engineer`

// Escrtura de String largos
/*
    Las rosas son rojas,
    Las violetas son azules,
    Caracter inesperado,
    En la lines 86.
*/

const poema = 'Las rosas son rojas,\n' + 
              'Las violetas son azules,\n' +
              'Caracter inesperado,\n' +
              'En la lines 86.'

console.log(poema)              

const poema2 = 'Las rosas son rojas,\n\
Las violetas son azules,\n\
Caracter inesperado\n\
En la linea 86.'

console.log(poema2)    

const poema3 = `Las rosas son rojas,
Las violetas son azules,
Caracter inesperado
En la linea 86.`

console.log(poema3)  
