const greeting = function (name) {
    return `Hi, ${name}`
}

// funcion flecha o arrow function --- explicit return

const newgreeting = (name) => {
    return `Hi, ${name}`
}

// funcion flecha o arrow function --- implicit return

const newgreetingimplicit = name => `Hi, ${name}`
const newgreetingimplicit2 = (name, lastname) => `Hi, ${name} and ${lastname}`

// Lexical binding

const finctionalcharacter = {
    name: 'Uncle ben',
    message1: function (message) {
        console.log(`${this.name} says: ${message}`)
    },
    message2: message => {
        console.log(`${this.name} says: ${message}`)
    }
}

finctionalcharacter.message1('with great power comes great responsability')
finctionalcharacter.message2('Beware of doctor octopus')
