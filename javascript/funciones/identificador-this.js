// Enlace implicito   --- implicit bliding

const house = {
    dogname: 'Rambo',
    dogGreeting: function () {
        console.log(`Hi, I'm ${this.dogname}`)
    }
}

house.dogGreeting()

// Enlace explicito   --- explicit bliding

function dogGreeting () {
    console.log(`Hi, I'm ${this.dogname}`)
}

const newhouse = {
    dogname: 'natasha',
}

dogGreeting.call(newhouse)


function newdogGreeting (propietario, direccion) {
    console.log(`Hi, I'm ${this.dogname} and live with ${propietario} on ${direccion}`)
}

const propietario = 'Marina'
const direccion = 'carrera 3 sur #2e-53'

newdogGreeting.call(newhouse, propietario, direccion)