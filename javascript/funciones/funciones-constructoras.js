const personalizedmessage = () => 'Goodbye everybody!'

function Rocket (name, ownmessage) {
    this.name = name
    this.launchMessage = ownmessage
}

const falcon9Rocket = new Rocket('Falcon 9', personalizedmessage)
const falconHeavyRocket = new Rocket('Falcon Heavy', 'see you soon !')

console.log(falcon9Rocket.name)
console.log(falcon9Rocket.launchMessage)


const RocketwithArrowFunction = (name, ownmessage) => ({
    name: name,
    launchMessage: ownmessage
})

const personalizedmessageForArrowFunction = () => 'succesful launch'
const falco9Rocket1 =  RocketwithArrowFunction('Falcon 9', personalizedmessageForArrowFunction)
console.group(falco9Rocket1)
console.group(falco9Rocket1.launchMessage())