// Explicit Type Casting
const string = '42'
const integer = parseInt(string)
console.log(integer)
console.log(typeof integer)

const stringDecimal = '3.14'
const float = parseFloat(stringDecimal)
console.log(float)
console.log(typeof float)

const binary = '1010'
const decimal = parseInt(binary, 2)
console.log(decimal)
console.log(typeof decimal)

// Implicit Type Casting
const suma = '5' + 3
console.log(suma)

const sumwithboolean = '3' + true
console.log(sumwithboolean)

const sumwithnumber = 2 + true
console.log(sumwithnumber)

const stringValue = '10'
const NumberValue = 10
const booleanValue = true
console.log('----------------------')
console.log(stringValue + stringValue) //concatena
console.log(stringValue + NumberValue) //concatena
console.log(stringValue + booleanValue) //concatena
console.log(NumberValue + stringValue) //concatena
console.log(NumberValue + NumberValue)  //suma
console.log(NumberValue + booleanValue) //suma
console.log(booleanValue + stringValue) //concatena
console.log(booleanValue + NumberValue) //suma
console.log(booleanValue + booleanValue) //suma
