const tipoSuscripcion = {
    free: 'Solo puedes tomar los cursos gratis',
    basic: 'Puedes tomar todos los cursos de platzi durante un mes',
    expert: 'Puedes tomar casi todos los cursos de platzi durante un año',
    expertDuo: 'Tu y alguien mas pueden tomar los cursos de platzi durante un año'
}


function conseguirSuscripcion (tipoSuscripcion) {
    if (conseguirSuscripcion[tipoSuscripcion]){
        console.log(conseguirSuscripcion[tipoSuscripcion]);
        return;
    }
    console.warn('este tipo de suscripcion no existe')
}

conseguirSuscripcion([tipoSuscripcion('free')])