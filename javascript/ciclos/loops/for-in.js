// for in  --> objetos 
//propiedades = valor
//consta de propiedades que a su vez tienen un valor
//array, string
//item
//for (variable in objeto) {
//codigo
//}

const listaCompras = {
    manzana: 5,
    pera: 3,
    Naranja: 2,
    uva: 1
}
for (fruta in listaCompras){
    console.log(fruta)
}

for (fruta in listaCompras){
    console.log(`${fruta} : ${listaCompras[fruta]}`)
}

for (fruta of listaCompras){
    console.log(fruta)
}