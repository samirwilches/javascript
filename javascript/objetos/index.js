/*
que es un objeto?

estructura de datos

llave o key ///   valor o value

objeto {
    propiedad: valor,
    propiedad: valor,
    propiedad: {
        agregar propiedades a esta propiedad
        propiedad: valor,
        propiedad: valor
    }
    Metodos (funcion anonima o arrow function) {
        cual es el codigo o la funcion que yo voy a generar
        ej: console.log("mensaje que quiero mostrar en este metodo")
    }
}

*/

// Ejemplo

const persona = {
    nombre: 'Samir',
    apellido: 'Wilches',
    edad: 23,
    direccion : {
        carrera: "3 sur",
        calle: "2 Este 53",
        barrio: "girardot"
    },
    saludar(){
        console.log(`Hola mi nombre es ${persona.nombre} y mi apellido es ${persona.apellido}`)
    }
}

console.log(persona)
console.log(persona.saludar())