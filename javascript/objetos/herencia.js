class Animal {
    constructor(nombre, tipo) {
        this.nombre = nombre,
        this.tipo = tipo
    }
    emitirSonido() {
        console.log("El animal emite un sonido")
    }
}

class Perro extends Animal{        // la palabra extends sirve para anidar una clase con la otra o traer valores de una clase a otra
    constructor(nombre, tipo, raza) {
        super(nombre, tipo) // con esta palabra super podemos traer las propiedades de otra clase
        this.raza = raza
    }
    emitirSonido () {
        console.log("El perro ladra")
    }
    correr () {
        console.log(`${this.nombre} corre alegremente`)
    }
}

const perro1 = new Perro("rambo", "perro", "criollo")

console.log(perro1)

perro1.correr()