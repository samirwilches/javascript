
function ganadorTorneo (competidores, resultados) {
    const score = {}
        let winner = ''

    
    for( let i = 0; i < competidores.length; i++) {
    // const home = competidores[i][0]
    const [home, away] = competidores[i]
    const equipoGanador = resultados[i] === 0 ? away : home

    score[equipoGanador] = (score[equipoGanador] || 0) + 3

        if (!winner || score[equipoGanador] > score[winner]) {
            winner = equipoGanador
        }        
    }
    return winner
}

const competidores = [
    ['Javascript', 'C#'],
    ['C#', 'Python'],
    ['Python', 'Javascript']
]

const resultados = [0, 0, 1]

console.log(ganadorTorneo(competidores, resultados))