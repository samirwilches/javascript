// How to create an array ?

// 1. new array() or array()

const frutas = Array('manzana', 'banano', 'naranja')
console.log(frutas)

const unNumero = Array(10)
console.log(unNumero)

const numero = Array(1, 2, 3, 4, 5)
console.log(numero)

// 2. Array literal syntax

const unNumeros = [4]
console.log(unNumeros)

const arrayVacio = []
console.log(arrayVacio)

const deportes = ['futbol', 'ciclismo', 'correr']
console.log(deportes)

const Ingredientes = [
    'Flour',
    true,
    2,
    {
        ingredente: 'leche', quantity: '1 vaso'
    },
    false
]
console.log(Ingredientes)

// acceder a los elementos dentro del array

const primeraFruta = frutas[0]
console.log(primeraFruta)

// tamaño de un array

const numerodefrutas = frutas.length
console.group(numerodefrutas)


// mutabilidad de arrays

frutas.push('fresas')
console.log(frutas)


// inmutabilidad de arrays

const newfrutras = frutas.concat(['pera', 'kiwi'])
console.log(frutas)
console.log(newfrutras)

// verificar si un array es un array

const esArray = Array.isArray(frutas)
console.log(esArray)

// ejercicio practico, la suma de todos los elementos de un array

const numerosArray = [1, 2, 3, 4]
sum = 0

for (let i = 0; i < numerosArray.length; i++) {
    sum += numerosArray[i]
    console.log(sum)
}

const objeto = {
    name: 'samir',
    apellido: 'wilches' 
}

function sumar (a, b){
    return a + b
}

const ObjectsList = [
    {
        name: 'samir',
        lastname: 'wilches',  
        color: 'azul',
        age: 15      
    },
    {
        name: 'yonathan',
        lastname: 'wilches',  
        color: 'azul',
        age: 15  
    }
]

for (let i = 0; i < ObjectsList.length; i++){
    console.log("el resultado es: " + ObjectsList[i])
}

