const personsList = [
    {
        nombre: 'samir',
        apellido: 'wilches',
        dias_trabajados: 30,
        valor_dia: 30000
    },
    {
        nombre: 'marina',
        apellido: 'muñoz',
        dias_trabajados: 30,
        valor_dia: 30000
    },
    {
        nombre: 'raul',
        apellido: 'tumbia',
        dias_trabajados: 30,
        valor_dia: 30000
    },
]
let pay_days = 0;
let nombres = '';
let apellidos = '';
for (let i = 0; i < personsList.length; i++){
    nombres = nombres + ' ' + personsList[i].nombre
    apellidos = apellidos + ' ' + personsList[i].apellido
    pay_days = pay_days + (personsList[i].dias_trabajados * personsList[i].valor_dia)
}
console.log(`el nombre de las personas es: ${nombres}, el apellido de las personas es ${apellidos} y el valor de la nomina es: ${pay_days}`)

const multiplicando = personsList.reduce((sum, item) => sum + (item.dias_trabajados * item.valor_dia), 0)
console.log('el valor de la nomina es: ', multiplicando)


// utilizando map para agregar nuevas propiedades
const newArrayList = personsList.map((item) => {
    return {
        ...item,
        curso: {
            materia: 'ingles',
            horas: 2
        }
    }
})
console.log(personsList)
console.log(newArrayList)

//recorriendo el nuevo arreglo 
let cursos = ''; 
let tiempo = ''; 
for (let j = 0; j < newArrayList.length; j++){
    nombres = nombres + ' ' + newArrayList[j].nombre
    cursos = newArrayList[j].curso.materia
    tiempo = newArrayList[j].curso.horas
}
console.log(`el nombre de las personas es: ${nombres}, la materia de todos es ${cursos} y el total de horas del curso es: ${tiempo}`)

newArrayList.push(
    {
        nombre: 'yonathan',
        apellido: 'muñoz',
        dias_trabajados: 30,
        valor_dia: 30000,
        curso: {
            materia: 'español',
            horas: 3
        }
    }
)
console.log(personsList)
console.log('agregamos un nuevo objeto con el metodo push', newArrayList)

newArrayList.forEach(item => console.log('recorriendo el foreach', item))

const multiplicar = newArrayList.reduce((sum, item) => sum + item.curso.horas, 0)
console.log('la suma de horas dek curso es: ', multiplicar)