const suma = [1, 2, 3, 4]
let rta = 0
for (let i = 0; i < suma.length; i++){
    const rta1 = suma[i]
    rta = rta + rta1;
}

console.log('la respuesta de este ejercicio es: ' + rta)

const result = suma.reduce((rta, rta1) => rta + rta1, 0)
console.log('el valor del reduce es ' + result)